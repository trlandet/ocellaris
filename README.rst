Repository has moved
====================

This repository has moved to https://bitbucket.org/ocellarisproject/ocellaris

The documentation can `now be found here <https://www.ocellaris.org/>`_
